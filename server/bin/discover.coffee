_        = require 'lodash'
loopback = require 'loopback'
config   = (require '../datasources.json').mysql


config = _.chain config
          .extend username: config.user
          .omit ['connector', 'user', 'name']
          .value()

ds = loopback.createDataSource 'mysql', config

ds.discoverAndBuildModels 'department', visited: {}, associations: true, (err, models) ->
  models.Department.findOne {}, (err, department) ->
    if err
      return console.error err
    console.log 'Department: ', department
