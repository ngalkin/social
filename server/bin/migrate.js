var async      = require('asyncawait/async')
  , await      = require('asyncawait/await')
  , dataSource = require('../server').dataSources.mysql;


var migrate = async.cps(function() {
  await(dataSource.automigrate.bind(dataSource, 'department'));
  await(dataSource.automigrate.bind(dataSource, 'group'));
  await(dataSource.automigrate.bind(dataSource, 'User'));
  await(dataSource.automigrate.bind(dataSource, 'contact'));
});

migrate(function(err) {
  if (err) throw err;
  dataSource.disconnect();
});
