var ldap = require('ldapjs')
  , async = require('async')
  , app = require('../server')
  , config = app.get('services').ldap
  , User = app.models.contact
  , client = ldap.createClient({url: config.url});


function run(done) {
  async.waterfall([
    function(next) {
      client.bind(config.username, config.password, next)
    },
    function(result, next) {
      var i = 0
        , users = [];
      async.whilst(
        function() { return i < config.baseDns.length },
        function(next) {
          console.log(config.baseDns[i]);
          client.search(config.baseDns[i], {filter: config.filter, scope: 'sub', attributes: ['cn', 'mail', 'sAMAccountName']}, function (err, result) {
            if (err) return next(err);
            result.on('error', next);

            result.on('searchEntry', function(entry) {
              users.push(entry.object);
            });
            result.on('end', function() {
              next(null, users)
            })
          }) && i++;
        },
        function(err) {
          next(err, users.map(function(user) {
            return {
              fullName: user.cn,
              username: user.sAMAccountName,
              email: user.mail,
              password: '123',
              created: new Date()
            }
          }))
        }
      )
    },
    function(users, next) {
      async.whilst(
        function() { return users.length },
        function(next) {
          var user = users.pop();
          console.log(users.length);
          User.create(user, function(err) {
            next()
          })
        },
        next
      )
    }
  ], done);
}

run(function(err) {
  if (err) throw err;
  client.unbind(function(err) {
    if (err) throw err;
  });
});

