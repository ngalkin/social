module.exports = function(app) {
  var router = app.loopback.Router();

  router.get('/test', function (req, res) {
    res.render('index', {loginFailed: false});
  });
  app.use(router)
};
