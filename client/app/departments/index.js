(function (angular) {
  angular
    .module('app.departments', ['ui.router', 'lbServices', 'app.departments.controllers'])
    .config(Config);

  Config.$inject = ['$stateProvider', '$urlRouterProvider'];

  function Config($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('departments', {
        url: '/departments',
        templateUrl: '/partials/departments/_index.html',
        controller: 'DepartmentsListController',
        controllerAs: 'ListCtrl'
      })
      .state('departments.view', {
        url: '/{id:[0-9]+}',
        templateUrl: '/partials/departments/view.html',
        controller: 'DepartmentsViewController',
        controllerAs: 'ViewCtrl'
      })
      .state('departments.add', {
        url: '/add',
        templateUrl: '/partials/departments/add.html',
        controller: 'DepartmentsAddController',
        controllerAs: 'AddCtrl'
      })
      .state('departments.edit', {
        url: '/{id:[0-9]+}/edit',
        templateUrl: '/partials/departments/edit.html',
        controller: 'DepartmentsEditController',
        controllerAs: 'EditCtrl'
      })
      .state('departments.remove', {
        url: '/{id:[0-9]+}/remove',
        templateUrl: '/partials/departments/remove.html',
        controller: 'DepartmentsRemoveController',
        controllerAs: 'RemoveCtrl'
      });
    $urlRouterProvider.otherwise('/departments')
  }
})(window.angular);
