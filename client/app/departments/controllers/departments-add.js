(function (angular) {
  angular
    .module('app.departments.controllers.add', ['lbServices'])
    .controller('DepartmentsAddController', DepartmentsAddController);


  DepartmentsAddController.$injector = ['$state', 'DepartmentService', 'Contact'];
  function DepartmentsAddController($state, DepartmentService, Contact) {
    var self = this;
    self.$state = $state;
    self.ModelService = DepartmentService;
    self.contacts = Contact.find({filter: {order: 'fullName ASC'}});
  }

  DepartmentsAddController.prototype.cancel = function() {
    var self = this;
    self.$state.go('departments');
  };

  DepartmentsAddController.prototype.submit = function() {
    var self = this;
    return self.ModelService.upsert(self.item, function(value) {
      self.$state.go('departments.view', value);
    })
  };
})(window.angular);

