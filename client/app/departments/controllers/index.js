(function (angular) {
  angular
    .module('app.departments.controllers', [
      'app.departments.controllers.list',
      'app.departments.controllers.view',
      'app.departments.controllers.add',
      'app.departments.controllers.edit',
      'app.departments.controllers.remove'
    ])
})(window.angular);

