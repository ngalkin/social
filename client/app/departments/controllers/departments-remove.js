(function (angular) {
  angular
    .module('app.departments.controllers.remove', ['ui.bootstrap', 'lbServices'])
    .controller('DepartmentsRemoveController', DepartmentsRemoveController);


  DepartmentsRemoveController.$injector = ['$state', 'DepartmentService'];
  function DepartmentsRemoveController($state, DepartmentService) {
    var self = this;
    self.$state = $state;
    self.ModelService = DepartmentService;
    self.item =  DepartmentService.findById(self.$state.params.id);
  }


  DepartmentsRemoveController.prototype.cancel = function() {
    var self = this;
    self.$state.go('departments.view', self.item);
  };


  DepartmentsRemoveController.prototype.submit = function() {
    var self = this;
    return self.ModelService.remove(self.item.id, function() {
      self.$state.go('departments');
    });
  };
})(window.angular);

