(function (angular) {
  angular
    .module('app.departments.controllers.edit', ['lbServices'])
    .controller('DepartmentsEditController', DepartmentsEditController);


  DepartmentsEditController.$injector = ['$state', 'DepartmentService', 'Contact'];
  function DepartmentsEditController($state, DepartmentService, Contact) {
    var self = this;
    self.$state = $state;
    self.ModelService = DepartmentService;
    self.contacts = Contact.find({filter: {order: 'fullName ASC'}});
    DepartmentService.findById(self.$state.params.id, function(item) {
      self.item = item;
    });
  }

  DepartmentsEditController.prototype.cancel = function(item) {
    var self = this;
    self.$state.go('departments.view', item);
  };

  DepartmentsEditController.prototype.submit = function() {
    var self = this;
    self.ModelService.edit(self.item, function() {
      self.$state.go('departments.view', self.item);
    })
  };
})(window.angular);
