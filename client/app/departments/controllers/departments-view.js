(function (angular) {
  angular
    .module('app.departments.controllers.view', ['lbServices'])
    .controller('DepartmentsViewController', DepartmentsViewController);


  DepartmentsViewController.$injector = ['$state', 'DepartmentService'];
  function DepartmentsViewController ($state, DepartmentService) {
    var self = this;
    DepartmentService.findById($state.params.id, function (item) {
      self.item = item
    });
  }
})(window.angular);

