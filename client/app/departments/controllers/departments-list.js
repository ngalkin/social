(function (angular) {
  angular
    .module('app.departments.controllers.list', ['lbServices', 'app.departments.services.department'])
    .controller('DepartmentsListController', DepartmentsListController);


  DepartmentsListController.$injector = ['DepartmentService'];
  function DepartmentsListController (DepartmentService) {
    var self = this;
    self.items = DepartmentService.all();
  }
})(window.angular);
