(function (angular, _) {
  angular
    .module('app.departments.services.department', ['lbServices'])
    .service('DepartmentService', DepartmentService);


  DepartmentService.$injector = ['Department'];
  function DepartmentService(Department) {
    var self = this;
    self.Model = Department;
    self.items = this.all()
  }

  DepartmentService.prototype.all = function() {
    var self = this;
    if (_.isEmpty(self.items)) {
      self.items = self.Model.find({filter: {include: ['groups', 'teamLead']}});
    }
    return self.items;
  };

  DepartmentService.prototype.findById = function(id, done) {
    return this.Model.find({filter: {where: {id: id}, include: ['groups', 'teamLead']}}).$promise.then(function(result) {
      done(result[0])
    });
  };

  DepartmentService.prototype.upsert = function(item, done) {
    var self = this;
    self.Model.upsert(item, function(item) {
      item.groups = [];
      (self.items || []).push(item);
      done(item)
    })
  };

  DepartmentService.prototype.edit = function(department, done) {
    var self = this;
    delete  department.teamLead;
    delete  department.groups;

    self.Model.upsert(department, function() {
      var index = _.findIndex(self.items || [], {id: department.id});
      self.findById(department.id, function(item) {
        self.items[index] = item;
        done(item)
      });
    })
  };

  DepartmentService.prototype.clear = function() {
    this.items = [];
  };


  DepartmentService.prototype.remove = function(id, done) {
    var self = this;
    self.Model.removeById({id: id}, function() {
      var item = _.find(self.items, {id: id});
      _.pull(self.items, item);
      done(item)
    })
  }
})(window.angular, window._);
