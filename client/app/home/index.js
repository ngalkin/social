(function (angular) {
  angular
    .module('app.home', ['ui.router', 'app.home.controllers.index'])
    .config(Config);

  Config.$inject = ['$stateProvider', '$urlRouterProvider'];

  function Config($stateProvider, $urlRouterProvider) {
    $stateProvider.state('home', {
      url: '/',
      templateUrl: '/partials/home/_index.html',
      controller: 'IndexController'
    });
    $urlRouterProvider.otherwise('/');
  }
})(window.angular);

