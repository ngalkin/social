(function (angular) {
  angular
    .module('app.home.controllers.index', ['lbServices'])
    .controller('IndexController', IndexController);


  IndexController.$inject = ['$scope', 'Department', 'Group', 'Contact'];
  function IndexController ($scope, Department, Group, Contact) {
    $scope.departments = Department.count();
    $scope.groups = Group.count();
    $scope.contacts = Contact.count();
  }
})(window.angular);
