var gulp    = require('gulp')
  , rename          = require('gulp-rename')
  , loopbackAngular = require('gulp-loopback-sdk-angular');


gulp.task('services', function () {
  gulp.src('./server/server.js')
    .pipe(loopbackAngular())
    .pipe(rename('lb-services.js'))
    .pipe(gulp.dest('./client/app'))
});

gulp.task('default', ['services']);
